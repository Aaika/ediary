import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'

Vue.use(Vuex);
export default new  Vuex.Store({
    state: {
        post:"https://e-kundoluk-flask-server.herokuapp.com/api/login/",
        StudentInfo:[],
        ParentInfo:[],
        SupervisorInfo:[],
        TeacherInfo:[],
        klass:'',
        Table:{},
        weeks:[],
    },
   getters:{
    table: state => {return state.Table},
     weeks:state => {return state.weeks}
  },
    mutations: {
      /**************************Student*****************************************/
        GetStudentInfo (state, payload) {
            state.StudentInfo = payload
        },
          KlassId (state, payload) {
            state.klass = payload
          },
      /***************************Parent****************************************/
        GetParentInfo (state, payload) {
            state.ParentInfo = payload
        },
      /**********************teacher*******************************************/
        GetTeacherInfo (state, payload) {
            state.TeacherInfo = payload
        },
      /**********************Supervisor*******************************************/
        GetSupervisorInfo (state, payload) {
            state.SupervisorInfo = payload
        },
        Ttable(state,day1,day2,day3,day4,day5){

        },

      TimeTable(state,TimeTable){
          state.Table=TimeTable;
          console.log('TimeTable',state.Table)
        },

        Weeks(state,payload){
            state.weeks=payload
        }
    },
    actions: {
      TimeTable({commit}, payload) {
        commit('TimeTable', payload)

      },
      WeekGradesAttendance({commit}) {
        let weeks =[]
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/week').then(response => {
          weeks = response.data;
          console.log('Week:',weeks);
          commit('Weeks', weeks)
        })

      }
    }
    //
    // actions: {
    //   initStore({commit}) {
    //     return this.$axios.$get('https://e-kundoluk-flask-server.herokuapp.com/api/timetable/klass/' + klass).then(data => {
    //       let tempObject = {}
    //       data.forEach(doc => {
    //         tempObject[doc._id] = doc
    //       });
    //       commit('SET_CATEGORIES', tempObject)
    //     }).catch(error => {
    //       console.log(error)
    //     })
    //   },
    //   TimeTable({commit},payload){
    //     let day1= [];
    //     let day2= [];
    //     let day3= [];
    //     let day4= [];
    //
    //
    //    payload.forEach((item)=>{
    //       if(item.day===1)
    //         {
    //           console.log(item.day)
    //           day1[item.day].push(item)
    //         }
    //      if(item.day===2)
    //      {
    //        console.log(item.day)
    //        day2[item.day].push(item)
    //      }
    //
    //
    //
    //
    //     });
    //     console.log(day1)
    //     console.log(day2)
    //   }
    //
    //   }

/*
export const state = () => ({
  list: {}
})

export const getters = {
  categories: state => state.list
}

export const mutations = {
  SET_CATEGORIES: (state, categories) => {
    state.list = categories
  }
}

export const actions = {
  initStore({commit}, payload) {
    return this.$axios.$get('/api/categories').then(data => {
      let tempObject = {}
      data.forEach(doc => {
        tempObject[doc._id] = doc
      });
      commit('SET_CATEGORIES', tempObject)
    }).catch(error => {
      console.log(error)
    })
  },
  update({commit, state , dispatch}, payload) {
    let form = new FormData();
    form.append('file' , payload.file);
    form.append('fileDetail' , payload.fileDetail);
    form.append('form', JSON.stringify(payload.form));
    return this.$axios.$put('/api/category', form).then((data) => {
      dispatch('initStore')
    })
  },
  del({commit, state}, payload) {
    console.log(payload)
    return this.$axios.$delete('/api/category/' + payload.id).then(() => {
      let tempObject = Object.assign({}, state.list)
      delete tempObject[payload.id]
      commit('SET_CATEGORIES', tempObject)
    })
  },
  add({commit, state}, payload) {
    let form = new FormData();
    form.append('file' , payload.file);
    form.append('fileDetail' , payload.fileDetail);
    form.append('form', JSON.stringify(payload.form));
    this.$axios.$post('/api/category/new', form).then(data => {
      let tempObject = Object.assign({}, state.list)
      tempObject[data._id] = data
      commit('SET_CATEGORIES', tempObject)
    })
  }
}
*/

})
